// on recupere les grandes parties (boutons) 
for (var i = 1 ; i<4 ; i++)  {
    let partie = document.getElementById(i);
    const listesSousparties = document.getElementById(`list${i}`);
   // au clic, les boutons vont faire s'afficher (display block) la sous partie correspondante 
   partie.addEventListener("click", () => {
    if (listesSousparties.style.display === "none" || listesSousparties.style.display === "") {
      listesSousparties.style.display = "block";
    } else {
      listesSousparties.style.display = "none";
    }
  });
}

  // ici, on va rendre visible le contenu des sous parties !
  var  sousPartiesTitles = document.getElementsByClassName('titresouspartie');
  var sousParties = document.getElementsByClassName('souspartie');
  var sousPartie = document.getElementsByClassName('contenu-sous-partie');
  // à chaque mouseover, on va display le contenu 
  for (let i = 0 ; i < sousParties.length ; i++) {

    // Masquer toutes les sous-parties avant d'afficher la sous-partie survolée


    sousParties[i].addEventListener("mouseover", (event) => {
  // Masquer toutes les sous-parties avant d'afficher la sous-partie survolée
  for (let j = 0; j < sousPartie.length; j++) {
    
    sousPartie[j].style.display = 'none';
    sousPartiesTitles[j].style.color = ''; // Reset the color for all sousParties

  }
  
  sousPartie[i].style.display = 'block';
  sousPartiesTitles[i].style.color = 'rgb(171, 185, 214)'; // Change the color to red when mouse is over
});

}
  
  // pour rendre display : none le contenu, un bouton "close" est ajouté (le x)
  var closeButton = document.getElementsByClassName('close');

  for (let i = 0; i < closeButton.length; i++) {
    closeButton[i].addEventListener("click", () => {
      sousPartie[i].style.display = 'none';
      
    });
  }